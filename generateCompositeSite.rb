#! /usr/bin/env ruby
# Copyright (C) 2013,2014 PizzaFactory Project
# Copyright (C) 2013 Monami-ya LLC, Japan.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

require 'inifile'
require 'aws-sdk'
require 'rexml/document'

VERBOSE=1

ini = IniFile.load(ENV['HOME'] + "/.aws/config")
section = 'default'
artifacts = []
contents = []

$uri = URI.parse(ARGV[0])
raise 'Scheme must be "s3"' if $uri.scheme != 's3' 

siteName = ARGV[1]
#==============================================================
s3 = AWS::S3.new(
  :access_key_id => ini[section]['aws_access_key_id'],
  :secret_access_key => ini[section]['aws_secret_access_key'],
  :region => ini[section]['region']
)

bucket = s3.buckets[$uri.host]
prefix = $uri.path[1..-1].gsub(/\/+$/, "")

bucket.objects.with_prefix(prefix).each do |file|
  artifacts << file.key if file.key =~ /artifacts.jar$/
  contents  << file.key if file.key =~ /content.jar$/
end

#==============================================================

def genCompositeXML(name, type, array)
  instruction = type.split('.')[-1]
  instruction = instruction[0].downcase + instruction[1..-1]

  doc = REXML::Document.new
  doc  << REXML::XMLDecl.new('1.0', 'UTF-8')
  doc << REXML::Instruction.new(instruction, "version='1.0'")
  repository = doc.add_element('repository', { 'name' => name, 'type' => type })
  properties = repository.add_element('properties', { 'size' => '1' })
  properties.add_element('property', { 'name' => 'p2.timestamp', 'value' => Time.now.to_i * 1000 })
  children = repository.add_element('children', { 'size' => array.length })
  array.each { |location| children.add_element('child', { 'location' => 'http://' + $uri.host + '/' + location }) }
  return doc
end

def verboseLog(msg)
  STDERR.puts msg if VERBOSE == 1
end

compositeContent = "compositeContent.xml"
compositeArtifacts = "compositeArtifacts.xml"

File::open(compositeContent, "w") {|f|
  f.puts genCompositeXML(siteName, "org.eclipse.equinox.internal.p2.metadata.repository.CompositeMetadataRepository", contents)
}

File::open(compositeArtifacts, "w") {|f|
  f.puts genCompositeXML(siteName, "org.eclipse.equinox.internal.p2.metadata.repository.CompositeArtifactRepository", artifacts)
}

[ compositeContent, compositeArtifacts ].each { |xmlFile|
  object = bucket.objects[prefix + "/" + xmlFile]
  object.write(Pathname.new(xmlFile))
  verboseLog "Wrote #{xmlFile} to #{prefix}"
}
