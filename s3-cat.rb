#! /usr/bin/env ruby
# Copyright (C) 2013,2014 PizzaFactory Project
# Copyright (C) 2013 Monami-ya LLC, Japan.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

require 'inifile'
require 'aws-sdk'

ini = IniFile.load(ENV['HOME'] + "/.aws/config")
section = 'default'

uri = URI.parse(ARGV[0])
raise 'Scheme must be "s3"' if uri.scheme != 's3'

#==============================================================
s3 = AWS::S3.new(
  :access_key_id => ini[section]['aws_access_key_id'],
  :secret_access_key => ini[section]['aws_secret_access_key'],
  :region => ini[section]['region']
)

bucket = s3.buckets[uri.host]
object = bucket.objects[uri.path[1..-1]]

object.read { |chunk|
  print chunk
}

